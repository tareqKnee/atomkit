@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
        <div style="visibility: hidden;" class="alert alert-success">
                <span id="success"></span>
            </div>
            <div style="visibility: hidden;" class="alert alert-danger">
                    <span id="error"></span>
                </div>
        @if(session('success'))
        <div class="alert alert-success">
            <span>{{session('success')}}</span>
        </div>
        @elseif(session('error'))
        <div class="alert alert-danger">
                <span>{{session('error')}}</span>
            </div>
        @endif
        <form method="POST" action="{{route('checkout')}}">
                @csrf
        <table id="cart" class="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th style="width:50%">Product</th>
                                <th style="width:10%">Price</th>
                                <th style="width:8%">Quantity</th>
                                <th style="width:22%" class="text-center">Subtotal</th>
                                <th style="width:10%"></th>
                            </tr>
                        </thead>
                        @php 
                        $sum = 0;
                        $quantity = 0;
                        @endphp
                        <tbody>
                                @foreach($list as $product)
                            <tr id="trash_{{$product->id}}">
                                <input type="hidden" name="ids[]" value="{{$product->product_id}}">
                                <input type="hidden" name="id[]" value="{{$product->id}}">
                                <td data-th="Product">
                                    <div class="row">
                                        <div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
                                        <div class="col-sm-10">
                                            <h4 class="nomargin">{{$product->name}} </h4>
                                            <p>{{$product->description}}</p>
                                        </div>
                                    </div>
                                </td>
                                @php 
                                $sum = $sum + $product->product->price;
                                $quantity = $quantity + $product->quantity;
                                @endphp
                                <td data-th="Price">{{$product->product->price}}</td>
                                <td data-th="Quantity">
                                    <input type="number" class="form-control text-center" value="{{$product->quantity}}" disabled>
                                </td>
                                <td data-th="Subtotal" class="text-center">{{$product->product->price * $product->quantity}}</td>
                                <td class="actions" data-th="">
                                    <a id="{{$product->id}}" class="btn btn-danger btn-sm trash"><i id="{{$product->id}}"class="fa fa-trash" aria-hidden="true"></i></a>								
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                    <tfoot>
                                            <tr class="visible-xs">
                                                    <td><a href="{{route('home')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                                    <td colspan="2" class="hidden-xs"></td>
                                                <td class="hidden-xs text-center"><strong>Total ${{$sum}}</strong></td>
                                                <input type="hidden" name="sum" class="form-control text-center" value="{{$sum}}">
                                                <input type="hidden" name="quantity" class="form-control text-center" value="{{$quantity}}">
                                                <td><button type="submit" class="btn btn-success btn-block">Checkout <i class="fas fa-angle-right"></i></button></td>
                                            </tr>
                                        </tfoot>
                                </tr>
                        </tbody>
                    </table>
                </form>
    </div>
@endsection
@section('styles')
<style>
    .table>tbody>tr>td, .table>tfoot>tr>td{
    vertical-align: middle;
}
@media screen and (max-width: 600px) {
    table#cart tbody td .form-control{
		width:20%;
		display: inline !important;
	}
	.actions .btn{
		width:36%;
		margin:1.5em 0;
	}
	
	.actions .btn-info{
		float:left;
	}
	.actions .btn-danger{
		float:right;
	}
	
	table#cart thead { display: none; }
	table#cart tbody td { display: block; padding: .6rem; min-width:320px;}
	table#cart tbody tr td:first-child { background: #333; color: #fff; }
	table#cart tbody td:before {
		content: attr(data-th); font-weight: bold;
		display: inline-block; width: 8rem;
	}
	
	
	
	table#cart tfoot td{display:block; }
	table#cart tfoot td .btn{display:block;}
	
}
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
//add to cart
$('.trash').on('click', function (e) {
        var id = e.target.id;
        console.log(id);
        $.ajaxSetup({ headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },cache: false });
            $.ajax({
              url: "/deleteCart",
              type: 'POST',
              data: {id: id},
              cache: false,
            success: function(res) {
                console.log(res);
                $('#trash_'+id).remove();
                $('#error').css("visibility","hidden")
                $('#success').css("visibility","visible")
                $('#success').addClass('alert alert-success')
                $('#success').text("Successfully deleted!");
            },
    error: function (jqXHR, exception) {
        console.log(jqXHR);
        $('#error').css("visibility","visible")
        $('#success').css("visibility","hidden")
        $('#error').addClass('alert alert-danger')
        $('#error').text("Something went wrong!");
        console.log(jqXHR.responseJSON['message']);
        },
        });
    });
});
</script>
@endsection