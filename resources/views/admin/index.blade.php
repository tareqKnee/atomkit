@extends('admin.layouts.adminLayout')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-xl-3">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-primary pull-left">
                    <i class="ti-user text-primary"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark m-t-10"><b class="counter">
                        {{ $users->count() }}</b></h3>
                    <p class="text-muted mb-0">Total Registrations</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-3">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="ti-bookmark-alt text-success"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark m-t-10"><b class="counter">
                        {{ $users->count() }}</b></h3>
                    <p class="text-muted mb-0">Complete & Verified Registrations</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
