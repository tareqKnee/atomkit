@extends('admin/layouts/adminLayout')

@section('title', 'Add Cards')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 m-b-30 header-title">Add a new product</h4>

                <form class="form-horizontal" role="form" method="POST"
                enctype="multipart/form-data" action="{{route('postadd')}}">
                    {{ csrf_field() }}
                    @if (count($errors) > 0)
                        <div class="form-group row">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if(session('result'))
                        <div class="form-group row">
                            <div class="alert alert-success">
                                Product added successfully!</div>
                        </div>
                    @endif

                    <div class="form-group row">
                        <label for="name" class="col-3 col-form-label">Product name</label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="Product name" value="{{ old('name') }}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-3 col-form-label">Product description</label>
                        <div class="col-9">
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}"
                                   id="description" placeholder="Product description"
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="quanitity" class="col-3 col-form-label">Quantity</label>
                        <div class="col-9">
                            <input type="number" min="1" class="form-control" name="quantity" value="{{ old('quantity') }}"
                                   id="quantity"
                                   placeholder="Product quanitity">
                        </div>
                    </div>

                    <div class="form-group row">
                            <label for="price" class="col-3 col-form-label">Price per unit</label>
                            <div class="col-9">
                                <input type="number" min="1" class="form-control" name="price" value="{{ old('quantity') }}"
                                       id="price"
                                       placeholder="Product price">
                            </div>
                        </div>

                    <div class="form-group row">
                            <label for="image" class="col-3 col-form-label">Product image</label>
                            <div class="col-9">
                                <input type="file" class="form-control" name="image" value="{{ old('image') }}"
                                       id="image">
                            </div>
                        </div>

                    <hr>

                    <div class="form-group m-b-0 row">
                        <div class="offset-3 col-9">
                            <button type="submit" class="btn btn-success waves-effect waves-light">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection


