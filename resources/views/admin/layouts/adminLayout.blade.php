<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="/fav.png">

    <title>@yield('head-title', 'Products Admin Panel')</title>
    <link href="/admin/plugins/switchery/switchery.min.css" rel="stylesheet"/>
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/admin/css/style.css" rel="stylesheet" type="text/css">
    <link href="/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/admin/plugins/switchery/switchery.min.css" rel="stylesheet">
    <link href="/admin/plugins/colorpicker/colorpicker.css" rel="stylesheet">
    <link href="/admin/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">   
    <script src="/admin/js/modernizr.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

</head>


<body class="fixed-left">
<div id="app">
</div>
<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="{{ url('manager/admin') }}" class="logo">
                    <div class="logosvg" style="position: relative; top: 5px; display: inline-block;">
                        <svg version="1.1" id="UstaziLogo" width="25" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             viewBox="0 0 480.4 582.3" style="enable-background:new 0 0 480.4 582.3;"
                             xml:space="preserve">
<style type="text/css">
    .st0 {
        fill: #FFFFFF;
    }
</style>
                            <path class="st0" d=""/>
</svg>
                    </div>
                    <span>Products</span>
                </a>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <nav class="navbar-custom">

            <ul class="list-inline float-right mb-0">

                <li class="list-inline-item notification-list hide-phone">
                    <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                        <i class="mdi mdi-crop-free noti-icon"></i>
                    </a>
                </li>

                <li class="list-inline-item notification-list hide-phone">
                    <a class="nav-link waves-light waves-effect" href="{{ url('') }}" target="_blank"
                       id="btn-openWebsite">
                        <i class="mdi mdi-open-in-new noti-icon"></i>
                    </a>
                </li>

                <li class="list-inline-item dropdown notification-list">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown"
                       href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <img src="/users/{{ Auth::user()->profile_picture != Null ? Auth::user()->profile_picture : 'pp-100.jpg' }}"
                             alt="user" class="rounded-circle">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="mdi mdi-settings"></i> <span>Settings</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                           class="dropdown-item notify-item">
                            <i class="mdi mdi-logout"></i> <span>Logout</span>
                        </a>
                        <form id="logout-form" action="{{ url('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-light waves-effect">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
                <li class="hide-phone app-search">
                    <form role="search" action="#"  class="">
                        {{ csrf_field() }}
                        <input type="text" placeholder="Search..." value="{{ @$_POST['search_value'] }}"
                               class="form-control" name="search_value">
                        <input type="hidden" name="type" value="0">
                        <a href=""><i class="fa fa-search"></i></a>
                    </form>
                </li>
            </ul>

        </nav>

    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>
                    <li class="menu-title">Main</li>

                    <li>
                        <a href="{{ url('manager/admin') }}" class="waves-effect waves-primary"><i
                                    class="ti-home"></i><span> Dashboard </span></a>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect waves-primary">
                            <i class="ti-user"></i>
                            <span> Products </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('AddAdminProduct')}}">Add a product</a></li>
                        </ul>
                    </li>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title">@yield('title', 'PAGE TITLE')</h4>
                            <ol class="breadcrumb float-right">
                                @yield('breadcrumb')
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                @yield('content')
            </div>
            <!-- end container -->
        </div>
        <!-- end content -->

    </div>

</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- Plugins  -->
<script src="/admin/js/jquery.min.js"></script>
<script src="/admin/js/popper.min.js"></script><!-- Popper for Bootstrap -->
<script src="/admin/js/bootstrap.min.js"></script>

<script src="/admin/js/app.js"></script>
<script src="/admin/js/detect.js"></script>
<script src="/admin/js/fastclick.js"></script>
<script src="/admin/js/jquery.slimscroll.js"></script>
<script src="/admin/js/jquery.blockUI.js"></script>
<script src="/admin/js/waves.js"></script>
<script src="/admin/js/wow.min.js"></script>
<script src="/admin/js/jquery.nicescroll.js"></script>
<script src="/admin/js/jquery.scrollTo.min.js"></script>
<script src="/admin/plugins/switchery/switchery.min.js"></script>
<script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/admin/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="/admin/plugins/datatables/jszip.min.js"></script>
<script src="/admin/plugins/datatables/pdfmake.min.js"></script>
<script src="/admin/plugins/datatables/vfs_fonts.js"></script>
<script src="/admin/plugins/datatables/buttons.html5.min.js"></script>
<script src="/admin/plugins/datatables/buttons.print.min.js"></script>
<script src="/admin/plugins/datatables/buttons.colVis.min.js"></script>
<script src="/admin/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script src="/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/admin/plugins/switchery/switchery.min.js"></script>
<script src="/admin/plugins/colorpicker/bootstrap-colorpicker.js"></script>
<script src="/admin/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

<script>
    $('.datePicker').datepicker({
        format: 'yyyy-m-d 00:00:00'
    });
</script>
<script defer>
$(document).ready(function(){ 
  $("#booking-tab").click(function(e){
      e.preventDefault();
      $("#canceled-tab").removeClass("active");
      $("#finish-tab").removeClass("active");
      $("#pending-tab").removeClass("active");
      $("#booking").addClass("active");
  });
  $("#pending-tab").click(function(e){
      e.preventDefault();
      $("#finish").removeClass("active");
      $("#canceled").removeClass("active");
      $("#booking").removeClass("active");
      $("#pending").addClass("active");
  });
  $("#finish-tab").click(function(e){
      e.preventDefault();
      $("#canceled").removeClass("active");
      $("#pending").removeClass("active");
      $("#booking").removeClass("active");
      $("#finish").addClass("active");
  });
  $("#canceled-tab").click(function(e){
      e.preventDefault();
      $("#finish").removeClass("active");
      $("#pending").removeClass("active");
      $("#booking").removeClass("active");
      $("#canceled").addClass("active");
  });
});
</script>
<script defer>
(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
</script>
<!-- Custom main Js -->
<script src="/admin/js/jquery.core.js"></script>
<script src="/admin/js/jquery.app.js"></script>
@yield('scripts')
</body>
</html>