@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
        <div style="visibility: hidden;" class="alert alert-success">
            <span id="success"></span>
        </div>
        <div style="visibility: hidden;" class="alert alert-danger">
                <span id="error"></span>
            </div>
        <div id="products" class="row grid-group-item justify-content-center">
            <div class="item  col-xs-4 col-lg-4" style="margin-right: 30px;">
                <div class="thumbnail">
                        <img class="group list-group-image" style="position: relative;
                        float: left;
                        width:  400px;
                        height: 400px;
                        background-position: 50% 50%;
                        background-repeat:   no-repeat;
                        background-size:     cover;" src="/uploads/{{$product->image}}" alt="" />
                        <div class="caption">
                        <h4 class="group inner list-group-item-heading">
                            {{$product->name}} </h4>
                        <p class="group inner list-group-item-text">
                            {{$product->description}}</p>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p class="lead">{{$product->price}}</p>
                            </div>
                            <div class="col-xs-12 col-md-6">
                               <input type="number" id="{{$product->id}}_quantity" placeholder="Quantity"> <a class="btn btn-success add-to-cart" id="{{$product->id}}" href="#">Add to cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>  
</div>    
@endsection
@section('styles')
<style>
   .glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 200%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}

</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
//add to cart
$('.add-to-cart').on('click', function (e) {
        var id = e.target.id;
        var quantity = $('#'+id+'_quantity').val();
        $.ajaxSetup({ headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },cache: false });
            $.ajax({
              url: "/AddProduct",
              type: 'POST',
              data: {id,quantity},
              cache: false,
            success: function(res) {
                $('#success').css("visibility","visible")
                $('#error').css("visibility","hidden")
                $('#success').addClass('alert alert-success')
                $('#success').text("Successfully added!");
            },
    error: function (jqXHR, exception) {
        $('#error').css("visibility","visible")
        $('#success').css("visibility","hidden")
        $('#error').addClass('alert alert-danger')
        $('#success').addClass('alert alert-success')
        $('#error').text("Something went wrong!");
        console.log(jqXHR.responseJSON['message']);
        },
        });
    });
});
</script>
@endsection