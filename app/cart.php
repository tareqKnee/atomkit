<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    protected $guarded = ['id'];

    public function product(){
        return $this->belongsTo('App\products','product_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
