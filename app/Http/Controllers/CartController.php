<?php

namespace App\Http\Controllers;

use App\cart;
use App\products;
use DB;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function returnCart()
    {
        $list = cart::with('product')->where(['user_id' => Auth()->user()->id, 'status' => 1])->get();
        return view('cart', compact('list'));
    }

    public function checkout(Request $request)
    {
        $this->validate($request, [
            'ids.*' => 'required|exists:products,id',
            'id.*' => 'required|exists:carts,id',
            'sum' => 'required|integer',
            'quantity' => 'required|integer',
        ]);

        $cart = cart::where('status', 1)->whereIn('id', $request->id)->sum('quantity');
        if ($cart < $request->quantity || $cart > $request->quantity) {
            return back()->with('error', "Quantity isn't correct!");
        }
        $return = DB::transaction(function () use ($request) {
            $carts = cart::whereIn('id', $request->id)->update(['status' => 0]);
            for ($i = 0; $i < count($request->ids); $i++) {
                $c = cart::with('product')->where(['product_id' => $request->ids[$i], 'user_id' => Auth()->user()->id])->first();
                if ($c->quantity < $c->product->quantity) {
                    products::where('id', $request->ids[$i])->decrement('quantity', $c->quantity);
                } else {
                    $c->delete();
                }
            }
            return true;
        });
        if ($return) {
            return back()->with('success', "Checked out successfully!");
        } else {
            return back()->with('error', "Something went wrong!");
        }
    }

    public function removeCart(Request $request){
        $this->validate($request, [
            'id' => 'required|exists:carts,id',
        ]);

        $cart = cart::where(['user_id'=>Auth()->user()->id,'id'=>$request->id])->first();
        if($cart){
            $cart->delete();
            return response()->json([
                'success' => "deleted"
            ],200);
        }
        else{
            return response()->json([
                'error' => "something went wrong!"
            ],404);
        }
    }
}
