<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $all = products::where('quantity','>',0)->paginate(5);
        return view('home',compact('all'));
    }

    public function returnProduct($id)
    {
        $product = products::where('quantity','>',0)->where('id',$id)->first();
        return view('product',compact('product'));
    }
}
