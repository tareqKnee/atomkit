<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\products;
use Illuminate\Http\Request;

class AdminProductController extends Controller
{
    public function index()
    {
        return view('admin.products.add');
    }

    public function Add(Request $request)
    {
        //dd($request->image);
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
            'quantity' => 'required|integer|min: 1',
            'price' => 'required|integer|min: 1',
            'image' => 'required|mimes:png,jpg,jpeg|max:10000',
        ]);

        $file = $request->file('image');
        $file_name = $file->getClientOriginalName();
        $file_name = str_replace(' ','',$file_name);
        $file_name = $request->name . '_' . $file_name;
        if (file_exists(public_path('uploads/' . $file_name))) {
            return back()->with('error', ' File already exists with that name!');
        }
        $file->getClientOriginalName();
        $file->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $file->move($destinationPath, $file_name);
        products::create(['name' => $request->name, 'price' => $request->price, 'description' => $request->description, 'quantity' => $request->quantity, 'image' => $file_name]);
        return back()->with('result', '1');
    }

}
