<?php

namespace App\Http\Controllers;

use App\cart;
use App\products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function AddProduct(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:products,id',
            'quantity' => 'required|integer|min:1',
        ]);

        try {
            $product = products::findOrFail($request->id);
            if($product->quantity < $request->quantity){
                return response()->json([
                    'error' => 'Your quantity is more than what we have in stored!',
                ],422);
            }
            $add = cart::create(['user_id' => Auth()->user()->id, 'product_id' => $request->id, 'quantity' => $request->quantity]);
            return response()->json([
                'Successfully added!',
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => $e,
            ]);
        }
    }
}
