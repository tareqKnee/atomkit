<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            abort('404');
        } else {
            if (Auth::user()->role == 3) {
                return $next($request);
            } elseif (Auth::user()->role == 4) {
                return $next($request);
            } else {
                abort('404');
            }
        }

        abort('404');
    }
}
