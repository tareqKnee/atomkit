<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    protected $guarded = ['id'];
    
    public function cart(){
        return $this->hasMany('App\cart','product_id');
    }
}
