<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    //Product
    Route::post('/AddProduct','ProductsController@AddProduct')->name('AddProduct');
    
    //home
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/product/{id}','HomeController@returnProduct')->name('returnProduct');

    //cart 
    Route::get('/cart', 'CartController@returnCart')->name('cart');
    Route::post('/checkout','CartController@checkout')->name('checkout');
    Route::post('/deleteCart','CartController@removeCart')->name('removeCart');
});

// admin routes
Route::group(['prefix' => 'manager/admin', 'middleware' => 'admin'], function () {
    Route::get('/', function () {
        $users = \App\User::all();
        return view('admin.index', compact('users'));
    });

    Route::get('/Product/Add','Admin\AdminProductController@index')->name('AddAdminProduct');
    Route::post('/Product/Add','Admin\AdminProductController@add')->name('postadd');
});